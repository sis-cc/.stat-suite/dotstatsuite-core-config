@echo OFF

if "%NUGET_ROOT%"=="" goto WARN

rmdir /S/Q "%userprofile%\.nuget\packages\dotstat.config"
rmdir /S/Q "%TEMP%\DotStat.Config"
dotnet pack %~dp0\DotStat.Config -c Debug -o "%TEMP%\DotStat.Config"
move "%TEMP%\DotStat.Config\*.nupkg" "%NUGET_ROOT%"

@echo Userprofile = %userprofile%
@echo NUGET_ROOT = %NUGET_ROOT%

dir %NUGET_ROOT%

goto :eof

:WARN
echo -----------------------------------------
echo Please set NUGET_ROOT environment variable with the path to your local nuget packages
echo -----------------------------------------