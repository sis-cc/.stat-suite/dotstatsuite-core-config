# HISTORY

## v11.3.0
- [#482](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/482) Added new localized text for PITReleaseDate updated now and in the past
- [#768](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/768) Added messages for non-supported tune dsd and archive features combined with MariaDb engine


## v11.2.0
- [#268](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/268) Added new localized text for existing PIT warning
- [#640](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/640) Updated localized text for cleanup/msd
- [#710](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/710) Added localized keys for index type tune
- [#711](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/711) Added filepath specific error message localization


## v11.1.0
- [#695](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/695) Added new localized text for unhandled exceptions with no id 


## v11.0.0
- [#170](https://gitlab.com/sis-cc/dotstatsuite-documentation/-/issues/170) Updated LICENSE file
- [#528](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/528) Made date time localized messages reusable and generic for any parameter 
- [#673](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/673) Changed sumary of changes localized log message


## v10.3.0
- [#407](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/407) Added new localization text for the case when bulk copy fails with value length exceeding the maximum 
- [#596](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/596) Added new localized text for importing and transfering referential metadata referencing a DSD
allowed by SQL data type 
- [#628](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/628) Added localized text for canceled transaction on startup


## v10.2.0
- [#456](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/456) Changed localization messages for missing dimensions in CSV and non-defined attributes
- [#576](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/576) Added new localization text for time machine


## v10.1.0
- [#306](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/306) New localization text for confidential observations
- [#534](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/534) Added localization texts for data compression setting
- [#580](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/580) improved dsd reference error message
- Improved texts of AttributeDoesNotBelongToDataset and DimensionDoesNotBelongToDataset localizations
- Improved localization texts related to duplicated and overlapping coordinates


## v10.0.0
- [#95](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/95) Added new localization text for invalid codes of allowed CC
- [#144](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/144) Correction of link URL in maximum attribute length related messages
- [#352](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/352) Added localized text for metadata delete and merge actions
- [#464](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/464) Added new localized text for failed init/dataflow
- [#484](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/484) Text of MissingDimensionMetadataImport extended
- [#504](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/504) Added localization key error to save ACC
- [#505](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/505) Remove unecessary localized text
- [#518](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/518) Added new validation key 'NoTimeZoneProvidedForUpdatedAfter'
- [#541](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/541) Updated metadata dim validation message
- [#548](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/548) Enhanced error message for duplicated observations
- [#563](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/563) Moved private nuget feed to gitlab
- Updated license information
- Enhanced localized text of key 'SourceFileHasNoComponentsToProcess'


## v9.1.3
- [#450](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/450) Fix for non-unique component ID accross DSD & referenced MSD


## v9.1.2
- [#435](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/435) Added new localized text for missing referenced dimensions


## v9.1.0
- [#127](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/127) New Localized text for delete actions and changed overlapping localized text
- [#397](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/397) Added new localized validation elements 
- [#410](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/410) Messages updated for no DSD and no dataflow cases in init/allMappingsets method
- [#424](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/424) Changed DataObservationsProcessedDetails localized text


## v9.0.1
- [#379](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/379) Add MailSent
- [#6](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/6) Enhance 'DuplicatedRowsInStagingTable' error message
- [#382](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/382) Cleanup of orphaned codelists


## v9.0.0
- [#277](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/277) Improve user information for lengthy imports
- [#83](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/83) Upgrade from .NET Core 3.1 to .NET 6


## v8.2.1
- [#269](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/269) Correction of some transfer messages


## v8.2.0
- [#304](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/304) Add localized UnhandledExceptionOccurred 
- [#278](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/278) Add localized Transaction items and missing localization text ProcessingMetadataAttributes 


## v8.1.1
- [#321](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/321) Fix for issue of data transactions failing with dataflows supporting ref.metadata


## v8.1.0
- [#310](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/310) Add translations for cleanup
- [#316](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/316) Add localized HttpClientTimeOut 
- [#302](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/302) Enhanced message of InitDBObjectsOfDataflowSuccess 
- [#296](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/296) Correct the transfer log messages for Referential Metadata transactions
- [#293](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/293) Add a proper error description when uploading metadata file to a structure without annotation link to MSD
- [#296](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/296) Correct the transfer log messages for Referential Metadata transactions
- [#80](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/80) Add new localised settings for metadata imports 


## v8.0.0
- [#113](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/113) New localization keys added related to support of time at time dimension
- [#178](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/178) Added new localisation keys for file management
- [#212](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/212) Added new label for missing Primary measure representation warning  
- [#215](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/215) New localized warning for non-included components 
- [#223](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/223) Change localization text for bulk copy notifications
- [#231](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/231) Add localization for updated mappingsets


## v7.0.0
- [#198](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/198) update localization text  
- [#174](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/174) Create transaction item for init/allMappingsets method 
- [#123](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/123) Add new translation keys for validations  


## v6.0.0
- [#60](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/60) Csv reader validation error messages 
- [#124](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/124) Localizations for non-numeric measure type implementation 
- [#167](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/167) Localisations for db validation  
- [#57](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/57) Add localization key sdmx-ml groups before series 


## v5.0.0
- [#63](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/63)  New messages added for better error description during initialization of dataflows and mappingsets
- [#110](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/110) New messages for allowed content constraint check for attributes
- [#165](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/165) New messages for when creating mappingsets
- [#120](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/120) New messages for validating format of submissionStart and submissionEnd parameters
- [#154](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/154) New messages for added Unauthorized import label 
- [#49](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/49) New messages for Add localized messages for DSD fix function 
- [#143](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/143) Updated message for a more detailed import summary 
- [#120](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/120) New messages Add email parameter localization error msg 


## v4.1.1 2020-06-08
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/101
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/100
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/102


## v4.0.0 2020-06-08
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/1
 - https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/94
 -  https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/29
 -  https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/78
 -  https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/83
 -  https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/93
 -  https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/48


## v3.0.1 2020-0-17
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/47
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/2
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/62
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/65


##  v2.1.6 2020-01-22
- sis-cc/.stat-suite/dotstatsuite-core-common/issues/102