# Content of this repository
The purpose of this repository is to have a common place for configuration files, used in multiple projects of the [dotstatsuite-core](https://gitlab.com/sis-cc/.stat-suite). The type of configuration stored in this repository, does not contain sensitive information. Currently, it only stores the file localization.json, which contains the translations for logs and emails.

>  The content of this repository is not meant to be used as standalone application. 

>  **This library is publicly published as a nuget package named [DotStat.Config](https://www.nuget.org/packages/DotStat.Config)**

The following solutions have **direct dependencies** to this library:
*  [dotstatsuite-core-data-access](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access)
*  [dotstatsuite-core-transfer](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer)
*  [dotstatsuite-core-auth-management](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management)
*  [dotstatsuite-core-sdmxri-nsi-plugin](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin)